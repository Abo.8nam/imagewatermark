﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace imagewatermark.Helper
{
    public interface IUploadImage
    {
        string Upload(IFormFile Photo);
    }
}
