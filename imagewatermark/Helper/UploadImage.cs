﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace imagewatermark.Helper
{
    public class UploadImage : IUploadImage
    {
        private readonly IHostingEnvironment HostingEnvironment;

        public UploadImage(IHostingEnvironment HostingEnvironment)
        {
            this.HostingEnvironment = HostingEnvironment;
        }


        public string Upload(IFormFile Photo)
        {

            string uniqueFileName = null;
            if (Photo != null)
            {
                string uploadsFolder = Path.Combine(HostingEnvironment.WebRootPath, $"images/products");
                uniqueFileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(Photo.FileName);
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    Photo.CopyTo(fileStream);
                }
            }
            return $"images/products/{uniqueFileName}" ;
        }


    }
}
