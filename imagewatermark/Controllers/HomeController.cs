﻿using imagewatermark.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using imagewatermark.Controllers.api;

namespace imagewatermark.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHostingEnvironment _environment;

        public HomeController(ILogger<HomeController> logger,IHostingEnvironment environment)
        {
            _logger = logger;
            _environment = environment;
        }

        public IActionResult Index()
        {
            ViewBag.listimg = TempData["listimg"];
            return View();
        }

        public IFormFile iff { get; set; }
        public async Task<IActionResult> WaterMarkImage(List<IFormFile> fileToUpload)
        {
            // listimg for display after processing bellow
            List<string> listimg = new List<string>();

            ImageController imageController = new ImageController(_logger, _environment);
            //foreach on all imgaes uploaded
            foreach (var item in fileToUpload)
            {


                string uniqueFileName = await imageController.addLogo(item);
                // set paths to view in runtime in index
                listimg.Add(uniqueFileName);
            }
            TempData["listimg"] = listimg;
            return RedirectToAction("Index");
        }




        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
