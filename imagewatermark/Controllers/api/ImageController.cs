﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace imagewatermark.Controllers.api
{
    public class ImageController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHostingEnvironment _environment;
        public ImageController(ILogger<HomeController> logger, IHostingEnvironment environment)
        {
            _logger = logger;
            _environment = environment;
        }


        public IActionResult Index()
        {
            return View();
        }


        public async Task<string> addLogo(IFormFile item)
        {
            // get stream of image to >>>> convert it from <<<< IFormFile to Image >>>> (For Processing)
            var stream = new MemoryStream();
            await item.CopyToAsync(stream);

            using (Image image = Image.FromStream(stream, true, false))
            {

                // get image uploaded
                Image upImage = Image.FromStream(stream);


                // get image logo
                string logoImagepath = Path.Combine(_environment.WebRootPath, "Img/watermarklogo2.png");
                Image logoImage = Image.FromFile(logoImagepath);




                // get filePath to upload
                string uploadFolder = Path.Combine(_environment.WebRootPath, "ImagWatermark");
                string uniqueFileName = Guid.NewGuid().ToString() + "_" + item.FileName;

                string filePath = Path.Combine(uploadFolder, uniqueFileName);



                Bitmap tempbitmap = new Bitmap(new Bitmap(upImage));

                // set water mark
                using (Graphics g = Graphics.FromImage(tempbitmap))
                {

                    // resize the logo depend on uploaded images
                    #region resize the logo depend on uploaded images
                    int newWidth = 0, newHeight = 0;
                    int sourceWidth = logoImage.Width;
                    int sourceHeight = logoImage.Height;


                    // set height and width of logo
                    newWidth = tempbitmap.Width / 3;
                    newHeight = tempbitmap.Height / 2;



                    int sourceX = 0, sourceY = 0, destX = 0, destY = 0;
                    float nPercent = 0, nPercentW = 0, nPercentH = 0;

                    nPercentW = ((float)newWidth / (float)sourceWidth);
                    nPercentH = ((float)newHeight / (float)sourceHeight);
                    if (nPercentH < nPercentW)
                    {
                        nPercent = nPercentH;
                        destX = System.Convert.ToInt16((newWidth -
                                  (sourceWidth * nPercent)) / 2);
                    }
                    else
                    {
                        nPercent = nPercentW;
                        destY = System.Convert.ToInt16((newHeight -
                                  (sourceHeight * nPercent)) / 2);
                    }

                    int destWidth = (int)(sourceWidth * nPercent);
                    int destHeight = (int)(sourceHeight * nPercent);


                    Bitmap bmPhoto = new Bitmap(newWidth, newHeight,
                                  PixelFormat.Format64bppPArgb);

                    bmPhoto.SetResolution(logoImage.HorizontalResolution,
                                 logoImage.VerticalResolution);

                    Graphics grPhoto = Graphics.FromImage(bmPhoto);
                    //grPhoto.Clear(Color.Transparent);
                    grPhoto.InterpolationMode =
                        System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                    grPhoto.DrawImage(logoImage,
                        new Rectangle(destX, destY, destWidth, destHeight),
                        new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                        GraphicsUnit.Pixel);



                    // convert new logo from <<<< Bitmap to Image >>>
                    Image logo2 = (Image)bmPhoto;
                    #endregion


                    // set logo to uploaded image
                    g.DrawImage(logo2, new Point(tempbitmap.Width - (logo2.Width / 3), tempbitmap.Height - (tempbitmap.Height / 6)));


                    //save uploaded to server
                    tempbitmap.Save(filePath);

                }

                return uniqueFileName;
                //// set paths to view in runtime in index
                //listimg.Add(uniqueFileName);
            }
        }
    }
}
